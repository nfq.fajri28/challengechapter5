const express = require ('express')
const app = express()
const PORT = 3000
const routes = require("./routes")

app.set('view engine',  'ejs')

app.use(express.json())
app.use(express.urlencoded({ extended:true}))

app.get("/", (req,res) => {
    res.send ("Berhasil")
})

app.use(routes)

app.listen(PORT, () => {
    console.log (`Server menyala di PORT ${PORT}`);
})
