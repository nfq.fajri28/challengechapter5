const express = require("express")
const routes = express.Router()
const loginRoutes = require("./loginRoutes")
const gameRoutes = require("./gameRoutes")


routes.use("/login", loginRoutes)
routes.use("/game", gameRoutes)



module.exports = routes