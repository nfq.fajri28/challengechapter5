const express = require ("express")
const routes = express.Router ()

// routes.get("/", (req,res) => {
//     res.send("Welcome")
// })

routes.get("/", (req,res) => {
    res.render("loginPage")
})

routes.post("/", (req,res) => {
    const {email, password} = req.body
    
    const dataUser = require("../../database/database.json")

    dataUser.forEach(el => {
        for (let index=0; index < dataUser.length; index++) {
        let emailDB = dataUser[index].email
        let passwordDB = dataUser[index].password

        if (emailDB === email && passwordDB === password) {
            let idUser = dataUser[index].id
            res.redirect(`/game${idUser}`)
        }  
        }
    })
})


module.exports = routes